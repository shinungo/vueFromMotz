import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Dive from "../components/dive.vue";
import Navigation from "../components/navigation.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Dive
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,

  routes: [
    {
      path: "/diveDetail/:id",
      name: "diveDetail",
      component: Navigation
    }
  ]
});

export default router;
