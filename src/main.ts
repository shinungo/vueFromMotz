import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "./i18n";
import * as VueGoogleMaps from "vue2-google-maps";
//import axios from "axios";
//import VueAxios from "vue-axios";
//const keyFile = require("@/appKeys");

Vue.config.productionTip = false;
//Vue.use(VueAxios, axios);

//Vue.axios.get("static/config.json").then((r: any) => {
//  console.log(r.data)
//})

Vue.use(VueGoogleMaps, {
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
